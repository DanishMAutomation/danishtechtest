## TechTest Danish

This is the documentation for deployment and provisioning of the tech test submitted by Danish.

### Task breakdown
- Create a Vagrantfile that creates a single machine using this box:
https://vagrantcloud.com/puppetlabs/boxes/ubuntu-14.04-64-nocm
and installs the latest released version of your chosen configuration management tool.
- Install the nginx webserver via configuration management.
- Run a simple test using Vagrant's shell provisioner to ensure that nginx is listening on port 80
- Again, using configuration management, update contents of /etc/sudoers file so that Vagrant
user can sudo without a password and that anyone in the admin group can sudo with a
password.
- Make the solution idempotent so that re-running the provisioning step will not restart nginx
unless changes have been made
- Create a simple "Hello World" web application in your favourite language of choice.
- Ensure your web application is available via the nginx instance.
- Extend the Vagrantfile to deploy this webapp to two additional vagrant machines and then
configure the nginx to load balance between them.
- Test (in an automated fashion) that both app servers are working, and that the nginx is
serving the content correctly.

### Document structure

```
.
├── README.md
├── Vagrantfile
├── ansible
│   ├── dev
│   ├── files
│   │   └── authorized_keys
│   ├── playbook.yml
│   ├── roles
│   │   ├── nginx
│   │   │   ├── handlers
│   │   │   │   └── main.yml
│   │   │   ├── tasks
│   │   │   │   └── main.yml
│   │   │   └── templates
│   │   │       └── default.tpl
│   │   └── nginx_proxy
│   │       ├── handlers
│   │       │   └── main.yml
│   │       ├── tasks
│   │       │   └── main.yml
│   │       └── templates
│   │           └── default.tpl
│   └── vars
│       └── all.yml
├── index.html
├── keys
│   └── insecure_private_key
└── shell
    └── provision.sh
```

### Foreword

For the submission, virtualbox was used for managing vagrant environments and ansible was used as the provisioning tool.
The servers are loadbalanced using nginx and also host a simple 'hello world' web application in HTML.

### Deployment

#### Prerequisites

- Virtualbox 5.2.16 or higher
- Vagrant configured with any additional add-ons

#### Deployment

1. Clone the repository
```
git clone git@bitbucket.org:DanishMAutomation/danishtechtest.git
```
2. Run the command to start the servers and provisioning
```
vagrant up
```
