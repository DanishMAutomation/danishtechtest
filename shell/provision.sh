#!/bin/bash

cd /vagrant/ansible || exit

# Download ansible
apt-get -y update
apt-get -y install software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get -y update
apt-get -y install ansible sshpass

# Start provisioning
ansible-playbook -i dev --private-key /home/vagrant/.ssh/id_rsa playbook.yml

# Check if nginx is running on port 80
netstat -tulpn | grep :80
